package br.ufpr.tads.eventoclick

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_tela2.*
import org.parceler.Parcels

class Tela2Activity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_tela2)
        val name = intent.getStringExtra("name")
        val age = intent.getIntExtra("age", -1)
        val client = intent.getParcelableExtra<Client>("client")
        val person = intent.getSerializableExtra("person") as Person?
        val client2 = Parcels.unwrap<Client2?>(intent.getParcelableExtra("client2"))
        val person2 = intent.getParcelableExtra<Person2>("person2")
        textMessage.text = if (client != null){
            getString(R.string.object_output, client.name, client.code)
        }
        else if(person != null){
            getString(R.string.object_output, person.name, person.code)
        }
        else if (client2 != null){
            getString(R.string.object_output, client2.name, client2.code)
        }
        else if (person2 != null){
            getString(R.string.object_output, person2.name, person2.code)
        }
        else {
            getString(R.string.normal_output, name, age)
        }
    }
}
