package br.ufpr.tads.eventoclick

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.activity_tela2.*
import org.parceler.Parcels


class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        buttonToast.setOnClickListener(object : View.OnClickListener {
            override fun onClick(v: View?) {
                val text = editText.text.toString()
                Toast.makeText(this@MainActivity, text, Toast.LENGTH_SHORT).show()
            }
        })
        buttonActivity2.setOnClickListener {
            val intent = Intent(this, Tela2Activity::class.java)
            intent.putExtra("name", "Alex")
            intent.putExtra("age", 33)
            startActivity(intent)
        }
        buttonParcel.setOnClickListener {
            val intent = Intent(this, Tela2Activity::class.java)
            val client = Client(1,"Alex Kutzke")
            intent.putExtra("client", client)
            startActivity(intent)
        }
        buttonSerial.setOnClickListener {
            val intent = Intent(this, Tela2Activity::class.java)
            val person = Person(1,"Alex Kutzke Serializable")
            intent.putExtra("person", person)
            startActivity(intent)
        }
        buttonParceler.setOnClickListener {
            val intent = Intent(this, Tela2Activity::class.java)
            val client = Client2(1,"Alex Kutzke Parceler")
            intent.putExtra("client2", Parcels.wrap(client))
            startActivity(intent)
        }
        buttonParcelize.setOnClickListener {
            val intent = Intent(this, Tela2Activity::class.java)
            val person = Person2(1,"Alex Kutzke Parcelize")
            intent.putExtra("person2", person)
            startActivity(intent)
        }
    }
}
